package com.inin.util.storage;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.*;

@Component
public class DynamoDbStorage {

    private static final Logger LOGGER = LoggerFactory.getLogger(DynamoDbStorage.class);

    private AWSCredentials awsCredentials;
    private AmazonDynamoDBClient ddbClient;

    @PostConstruct
    public void init() {
        try {
            ddbClient = new AmazonDynamoDBClient();
        } catch(Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    @PreDestroy
    public void shutdown() {
        try {
            ddbClient.shutdown();
        } catch(Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    public void createTableClone(String source, String target, boolean cloneProvisioning, boolean cloneSecondaryIndexes) throws Exception {
        DescribeTableRequest describeSourceTableRequest = new DescribeTableRequest();
        DescribeTableResult describeSourceTableResult = null;
        describeSourceTableRequest.setTableName(source);
        try {
            describeSourceTableResult = ddbClient.describeTable(describeSourceTableRequest);
        } catch (AmazonServiceException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new Exception(String.format("Unknown source table %1$s", source), ex);
        }
        TableDescription sourceDescription = describeSourceTableResult.getTable();
        if (sourceDescription.getTableStatus().compareTo("ACTIVE") != 0) {
            LOGGER.error("Source table {} has status {}", source, sourceDescription.getTableStatus());
            throw new Exception(String.format("Source table %1$s has status %2$s", source, sourceDescription.getTableStatus()));
        }

        try {
            DescribeTableRequest describeTargetTableRequest = new DescribeTableRequest();
            describeTargetTableRequest.setTableName(target);
            DescribeTableResult describeTargetTableResult = ddbClient.describeTable(describeTargetTableRequest);
            LOGGER.error("Target table {} already exists", target);
            throw new Exception(String.format("Target table %1$s already exists", target));
        } catch (AmazonServiceException ex) {
            // This is what we want
        }

        // Get the required attribute types
        HashMap<String, AttributeDefinition> attributeNameMap = new HashMap<>();
        HashSet<String> attributeNamesUsed = new HashSet<>();
        for (AttributeDefinition attr : sourceDescription.getAttributeDefinitions()) {
            attributeNameMap.put(attr.getAttributeName(), attr);
        }

        // Get key info
        KeySchemaElement hashKey = null;
        KeySchemaElement rangeKey = null;
        for (KeySchemaElement schemaElement : sourceDescription.getKeySchema()) {
            if (schemaElement.getKeyType().compareTo("HASH") == 0) {
                hashKey = schemaElement;
            }
            if (schemaElement.getKeyType().compareTo("RANGE") == 0) {
                rangeKey = schemaElement;
            }
        }

        // Get provisioning
        ProvisionedThroughputDescription provisionedThroughput = sourceDescription.getProvisionedThroughput();

        // Build the create table request
        CreateTableRequest createTableRequest = new CreateTableRequest();
        createTableRequest.setTableName(target);
        createTableRequest.setKeySchema(sourceDescription.getKeySchema());

        ArrayList<AttributeDefinition> targetAttributes = new ArrayList<>();
        targetAttributes.add(attributeNameMap.get(hashKey.getAttributeName()));
        attributeNamesUsed.add(hashKey.getAttributeName());
        if (null != rangeKey) {
            targetAttributes.add(attributeNameMap.get(rangeKey.getAttributeName()));
            attributeNamesUsed.add(rangeKey.getAttributeName());
        }

        if (cloneProvisioning) {
            createTableRequest.setProvisionedThroughput(new ProvisionedThroughput()
                    .withReadCapacityUnits(provisionedThroughput.getReadCapacityUnits())
                    .withWriteCapacityUnits(provisionedThroughput.getWriteCapacityUnits())
                                                       );
        } else {
            createTableRequest.setProvisionedThroughput(new ProvisionedThroughput()
                    .withReadCapacityUnits((long)10)
                    .withWriteCapacityUnits((long)5)
            );
        }

        if (cloneSecondaryIndexes) {
            ArrayList<LocalSecondaryIndex> targetSecondaryIndexes = new ArrayList<>();
            for (LocalSecondaryIndexDescription siDesc : sourceDescription.getLocalSecondaryIndexes()) {
                targetSecondaryIndexes.add(new LocalSecondaryIndex()
                        .withIndexName(siDesc.getIndexName())
                        .withKeySchema(siDesc.getKeySchema())
                        .withProjection(siDesc.getProjection())
                                          );
                for (KeySchemaElement se : siDesc.getKeySchema()) {
                    if (!attributeNamesUsed.contains(se.getAttributeName())) {
                        targetAttributes.add(attributeNameMap.get(se.getAttributeName()));
                        attributeNamesUsed.add(se.getAttributeName());
                    }
                }
            }
            createTableRequest.setLocalSecondaryIndexes(targetSecondaryIndexes);
        }

        createTableRequest.setAttributeDefinitions(targetAttributes);

        CreateTableResult createTableResult = ddbClient.createTable(createTableRequest);

        int faaart = 6;

    }

    public long simpleCopyTable(String source, String target) throws Exception {
        DescribeTableRequest describeSourceTableRequest = new DescribeTableRequest();
        DescribeTableResult describeSourceTableResult = null;
        describeSourceTableRequest.setTableName(source);
        try {
            describeSourceTableResult = ddbClient.describeTable(describeSourceTableRequest);
        } catch (AmazonServiceException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new Exception(String.format("Unknown source table %1$s", source), ex);
        }
        TableDescription sourceDescription = describeSourceTableResult.getTable();
        if (sourceDescription.getTableStatus().compareTo("ACTIVE") != 0) {
            LOGGER.error("Source table {} has status {}", source, sourceDescription.getTableStatus());
            throw new Exception(String.format("Source table %1$s has status %2$s", source, sourceDescription.getTableStatus()));
        }

        DescribeTableRequest describeTargetTableRequest = new DescribeTableRequest();
        DescribeTableResult describeTargetTableResult = null;
        describeTargetTableRequest.setTableName(target);
        try {
            describeTargetTableResult = ddbClient.describeTable(describeTargetTableRequest);
        } catch (AmazonServiceException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new Exception(String.format("Unknown target table %1$s", target), ex);
        }
        TableDescription targetDescription = describeTargetTableResult.getTable();
        if (sourceDescription.getTableStatus().compareTo("ACTIVE") != 0) {
            LOGGER.error("Target table {} has status {}", source, targetDescription.getTableStatus());
            throw new Exception(String.format("Target table %1$s has status %2$s", target, targetDescription.getTableStatus()));
        }

        // Set up for source table scan
        long numSourceItems = sourceDescription.getItemCount();
        long numCopied = 0;
        ScanRequest scanRequest = new ScanRequest()
                .withTableName(source)
                .withLimit(25)
                ;
        Map<String, AttributeValue> lastKey = null;
        while (numCopied < numSourceItems) {
            if (null != lastKey) {
                scanRequest.setExclusiveStartKey(lastKey);
            }
            ScanResult scanResult = ddbClient.scan(scanRequest);

            BatchWriteItemRequest batchWriteItemRequest = new BatchWriteItemRequest();
            HashMap<String, List<WriteRequest>> batchRequests = new HashMap<>(1);
            ArrayList<WriteRequest> writeRequests = new ArrayList<>(scanResult.getCount());
            batchRequests.put(target, writeRequests);
            batchWriteItemRequest.setRequestItems(batchRequests);
            for (Map<String, AttributeValue> attr : scanResult.getItems()) {
                writeRequests.add(new WriteRequest().withPutRequest(new PutRequest().withItem(attr)));
            }

            BatchWriteItemResult batchWriteItemResult = ddbClient.batchWriteItem(batchWriteItemRequest);
            lastKey = scanResult.getLastEvaluatedKey();

            numCopied += scanResult.getCount();
        }
        return numCopied;
    }
}
