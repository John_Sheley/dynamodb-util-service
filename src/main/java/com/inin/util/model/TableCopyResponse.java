package com.inin.util.model;

public class TableCopyResponse {
    private boolean success;
    private String message;
    private long ms;
    private long copyCount;
    private double itemsPerSecond;

    public TableCopyResponse() {
    }

    public TableCopyResponse(boolean success, String message) {
        this.success = success;
        this.message = message;
        this.ms = 0;
        this.copyCount = 0;
        this.itemsPerSecond = 0;
    }

    public TableCopyResponse(boolean success, String message, long ms, long copyCount, double itemsPerSecond) {
        this.success = success;
        this.message = message;
        this.ms = ms;
        this.copyCount = copyCount;
        this.itemsPerSecond = itemsPerSecond;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getMs() {
        return ms;
    }

    public void setMs(long ms) {
        this.ms = ms;
    }

    public long getCopyCount() {
        return copyCount;
    }

    public void setCopyCount(long copyCount) {
        this.copyCount = copyCount;
    }

    public double getItemsPerSecond() {
        return itemsPerSecond;
    }

    public void setItemsPerSecond(double itemsPerSecond) {
        this.itemsPerSecond = itemsPerSecond;
    }
}
