package com.inin.util.model;

public class TableCreateCloneRequest {
    private String source;
    private String target;
    private boolean cloneProvisioning = true;
    private boolean cloneSecondaryIndexes = false;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public boolean isCloneProvisioning() {
        return cloneProvisioning;
    }

    public void setCloneProvisioning(boolean cloneProvisioning) {
        this.cloneProvisioning = cloneProvisioning;
    }

    public boolean isCloneSecondaryIndexes() {
        return cloneSecondaryIndexes;
    }

    public void setCloneSecondaryIndexes(boolean cloneSecondaryIndexes) {
        this.cloneSecondaryIndexes = cloneSecondaryIndexes;
    }
}
