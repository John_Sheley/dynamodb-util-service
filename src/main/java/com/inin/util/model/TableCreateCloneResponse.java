package com.inin.util.model;

public class TableCreateCloneResponse {
    private boolean success;
    private String message;
    private long ms;

    public TableCreateCloneResponse() {
    }

    public TableCreateCloneResponse(boolean success, String message) {
        this.success = success;
        this.message = message;
        this.ms = 0;
    }

    public TableCreateCloneResponse(boolean success, String message, long ms) {
        this.success = success;
        this.message = message;
        this.ms = ms;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getMs() {
        return ms;
    }

    public void setMs(long ms) {
        this.ms = ms;
    }
}
