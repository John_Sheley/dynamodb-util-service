package com.inin.util.controller;

import com.inin.util.model.TableCopyRequest;
import com.inin.util.model.TableCopyResponse;
import com.inin.util.model.TableCreateCloneRequest;
import com.inin.util.model.TableCreateCloneResponse;
import com.inin.util.storage.DynamoDbStorage;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

@Controller
@RequestMapping("/tables")
public class TableController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TableController.class);

    @Autowired
    private DynamoDbStorage dynamoDbStorage;

    @RequestMapping(value = "/createClone", method = {RequestMethod.POST})
    @ResponseBody
    public ResponseEntity<TableCreateCloneResponse> createClone(
            @RequestBody TableCreateCloneRequest tableCreateCloneRequest,
            HttpServletRequest request,
            HttpServletResponse response) {

        try {
            if (null == tableCreateCloneRequest.getSource()) {
                TableCreateCloneResponse r = new TableCreateCloneResponse(false, "Must specify a source table");
                LOGGER.error(r.getMessage());
                return new ResponseEntity<>(r, HttpStatus.BAD_REQUEST);
            }
            if (null == tableCreateCloneRequest.getTarget()) {
                TableCreateCloneResponse r = new TableCreateCloneResponse(false, "Must specify a target table");
                LOGGER.error(r.getMessage());
                return new ResponseEntity<>(r, HttpStatus.BAD_REQUEST);
            }
            LOGGER.info("Creating cloned table from {} to {}; {} provisioning, {} secondary indexes",
                    tableCreateCloneRequest.getSource(), tableCreateCloneRequest.getTarget(),
                    tableCreateCloneRequest.isCloneProvisioning() ? "using existing" : "ignoring existing",
                    tableCreateCloneRequest.isCloneSecondaryIndexes() ? "copying existing" : "ignoring existing");

            Date start = new Date();
            dynamoDbStorage.createTableClone(tableCreateCloneRequest.getSource(),
                    tableCreateCloneRequest.getTarget(),
                    tableCreateCloneRequest.isCloneProvisioning(),
                    tableCreateCloneRequest.isCloneSecondaryIndexes());
            Date end = new Date();

            LOGGER.info("Created assignedEvaluation");
            return new ResponseEntity<>(new TableCreateCloneResponse(true, null, end.getTime() - start.getTime()), HttpStatus.OK);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            return new ResponseEntity<>(new TableCreateCloneResponse(false, ex.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/copy", method = {RequestMethod.POST})
    @ResponseBody
    public ResponseEntity<TableCopyResponse> copy(
            @RequestBody TableCopyRequest tableCopyRequest,
            HttpServletRequest request,
            HttpServletResponse response) {

        try {
            if (null == tableCopyRequest.getSource()) {
                TableCopyResponse r = new TableCopyResponse(false, "Must specify a source table");
                LOGGER.error(r.getMessage());
                return new ResponseEntity<>(r, HttpStatus.BAD_REQUEST);
            }
            if (null == tableCopyRequest.getTarget()) {
                TableCopyResponse r = new TableCopyResponse(false, "Must specify a target table");
                LOGGER.error(r.getMessage());
                return new ResponseEntity<>(r, HttpStatus.BAD_REQUEST);
            }
            LOGGER.info("Copying table data from {} to {}", tableCopyRequest.getSource(), tableCopyRequest.getTarget());

            Date start = new Date();
            long count = dynamoDbStorage.simpleCopyTable(tableCopyRequest.getSource(), tableCopyRequest.getTarget());
            Date end = new Date();
            long ms = end.getTime() - start.getTime();
            double countPerSecond = ((double)count * 1000) / (double)ms;

            LOGGER.info("Copied");
            return new ResponseEntity<>(new TableCopyResponse(true, null, ms, count, countPerSecond), HttpStatus.OK);
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            return new ResponseEntity<>(new TableCopyResponse(false, ex.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }
}
