package com.inin.util.config;

import com.codahale.metrics.MetricFilter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.SharedMetricRegistries;
import com.codahale.metrics.Slf4jReporter;
import com.codahale.metrics.graphite.Graphite;
import com.codahale.metrics.graphite.GraphiteReporter;
import com.ryantenney.metrics.spring.config.annotation.EnableMetrics;
import com.ryantenney.metrics.spring.config.annotation.MetricsConfigurerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PreDestroy;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;


/**
 * Reports metrics of the service.
 */
@Configuration
@EnableMetrics()
public class MetricsConfiguration extends MetricsConfigurerAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(MetricsConfiguration.class);

    private GraphiteReporter reporter;
    private Slf4jReporter logReporter;

    @Override
    public MetricRegistry getMetricRegistry() {
        return SharedMetricRegistries.getOrCreate("springMetrics");
    }

    @Override
    public void configureReporters(MetricRegistry metricRegistry) {
        String host="unknown";
        logReporter = Slf4jReporter.forRegistry(metricRegistry)
                .outputTo(LOGGER)
                .build();
        logReporter.start(1, TimeUnit.MINUTES);

        try {
            /*
             *  We are going to use hostname to help identify which node generated the stats
             *   - Example metrics ID would :
             *   	kal.<<host_name>>.<<service_name>>.com.inin.configuration.controller.EdgeAdapter.get
             */
            host = java.net.InetAddress.getLocalHost().getHostName();
        }
        catch (UnknownHostException ex) {
            LOGGER.error("Unable to get host name for metrics.",ex);
        }

        Graphite graphite = new Graphite(new InetSocketAddress("graphite.inintca.com", 2003));
        reporter = GraphiteReporter.forRegistry(metricRegistry)
                .prefixedWith("kal."+host+".configuration")
                .convertRatesTo(TimeUnit.SECONDS)
                .convertDurationsTo(TimeUnit.MILLISECONDS)
                .filter(MetricFilter.ALL)
                .build(graphite);
        reporter.start(1, TimeUnit.MINUTES);
    }

    @PreDestroy
    public void stopReporters() {
        LOGGER.info("Shutting down metrics reporters");
        if (logReporter != null) {
            logReporter.stop();
        }
        if (reporter != null) {
            reporter.stop();
        }
    }
}