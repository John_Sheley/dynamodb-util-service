package com.inin.util.config;

import javax.servlet.Filter;

import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import com.codahale.metrics.servlet.InstrumentedFilter;


/*****
 * 
 * Used as the No XML Spring MVC Servlet initializer.
 * 
 * http://static.springsource.org/spring/docs/3.0.0.M3/reference/html/ch16s02.html
 * 
 * The most important thing here is that that the {@link com.inin.util.config.DispatcherServletInitializer#getServletConfigClasses()} method
 * Points to the Primary Spring MVC annotated Configuration class. In this case, it's the WebConfig.java configuration class
 *  
 * 
 * @author shawn
 *
 */
public class DispatcherServletInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return null;
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[]{WebConfig.class};
    }

    @Override
    protected String[] getServletMappings() {
    	return new String[] { "/" };
    }

    
    @Override
    protected Filter[] getServletFilters() {
        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        InstrumentedFilter instrumentedFilter = new InstrumentedFilter();
        characterEncodingFilter.setEncoding("UTF-8");
        return new Filter[] { characterEncodingFilter, instrumentedFilter};
    }
}
